#! /usr/bin/env python

"""
The program automatically find and convert ALL the *.fit files it
finds inside the specified "input_path" directory.
 CAUTION: the original files are not deleted to avoid possible mistakes
 You will need to delete them when you are sure that all files are convertedcorrectly
Made in IDL by V. Larionov 25.01.2004, translated to Python by S. Savchenko 27.08.2021
"""

import datetime
from pathlib import Path
import numpy as np
from astropy.io import fits
from astropy.time import Time
from astropy.coordinates import SkyCoord
from astropy.coordinates import AltAz
from astropy.coordinates import EarthLocation
from astropy.coordinates import get_moon
from astropy.coordinates import get_sun
from astropy import units
import time
import multiprocessing


# St Petersburg location
site_location = EarthLocation(lat=59.880412*units.deg, lon=29.824820*units.deg, height=0*units.m)

path_to_data = Path("data/20210821")

# Read tabob file
tabob = {}
for line in open("TABOB.TXT"):
    if line.startswith("#"):
        continue
    obj_name = line.split()[0]
    ra_hms = line.split()[1:4]
    dec_dms = line.split()[4:7]
    tabob[obj_name] = (ra_hms, dec_dms)


def process(fname):
    print(fname)
    hdu = fits.open(fname)
    header = hdu[0].header
    naxis1 = int(header["NAXIS1"])
    naxis2 = int(header["NAXIS2"])
    # Modify the main image keywords
    if "COMMENT" in header:
        del header["COMMENT"]
    header.insert("CCD-TEMP", ("XPIXSZ", 13), after=True)
    header.insert("CCD-TEMP", ("YPIXSZ", 13), after=True)
    header["EACHSNAP"] = header["EXPTIME"]
    header["TELESCOP"] = 'LX200 16"'
    if "BSCALE" in header:
        del header["BSCALE"]
    header["BSCALE"] = 1
    if "BZERO" in header:
        del header["BZERO"]
    header["BZERO"] = 0
    # Read date and time of observarion
    datobs = Time(header["DATE-OBS"])
    # Compute siderial time
    sidereal = datobs.sidereal_time(kind='apparent', longitude=site_location.lon)
    # Compute TJD and write it next to DATE-OBS keyword
    tjd = datobs.mjd + 0.5
    header.insert("DATE-OBS", ("TJD-OBS", tjd))

    # Find coordinates of the object using tabob
    for obj_name in tabob.keys():
        if fname.name.startswith(obj_name):
            ra_hms, dec_dms = tabob[obj_name]
            # Parse filter from the file name
            substring = fname.name[len(obj_name):-4]
            filter_string = "+".join(substring[:substring.index("S")].upper())
            # Save filter in the header
            header.insert("YBINNING", ("FILTER", filter_string), after=True)
            break
    else:
        ra_hms = (0, 0, 0)
        dec_dms = (0, 0, 0)
    # Save object name in the header
    header["OBJECT"] = fname.name[:fname.name.index("S")]
    # Parse frame index
    index = int(fname.name[fname.name.index("S")+1:-4])
    # Coordinates
    coord_string = f"{ra_hms[0]}h{ra_hms[1]}m{ra_hms[2]}s "
    coord_string += f"{dec_dms[0]}d{dec_dms[1]}m{dec_dms[2]}s"
    equatorial_coords = SkyCoord(coord_string, frame="fk5")
    hour_angle = sidereal - equatorial_coords.ra

    # Compute reference pixel related keywords
    cdelt1 = -0.0001869 * int(header["XBINNING"])
    cdelt2 = 0.0001869 * int(header["YBINNING"])

    header.insert("EACHSNAP", ("RADECSYS", "FK5"), after=True)
    header.insert("RADECSYS", ("EQUINOX", 2000.0), after=True)

    header.insert('RADECSYS', ('CTYPE1', 'RA---TAN'), after=True)
    header.insert('CTYPE1', ('CRVAL1', equatorial_coords.ra.deg), after=True)
    header.insert('CRVAL1', ('CRPIX1', naxis1/2), after=True)
    header.insert('CRPIX1', ('CDELT1', cdelt1), after=True)
    header.insert('CDELT1', ('CROTA1', 0), after=True)
    header.insert('CROTA1', ('CTYPE2', 'DEC--TAN'), after=True)
    header.insert('CTYPE2', ('CRVAL2', equatorial_coords.dec.deg), after=True)
    header.insert('CRVAL2', ('CRPIX2', naxis2/2), after=True)
    header.insert('CRPIX2', ('CDELT2', cdelt2), after=True)
    header.insert('CDELT2', ('CROTA2', 0), after=True)

    # Compute horizonthal coordinates of the object
    jd = tjd + 2400000
    frame = AltAz(obstime=datobs, location=site_location)
    horizonthal_coords = equatorial_coords.transform_to(frame)

    # Compute airmass
    airmass = 1/np.cos(np.radians(90-horizonthal_coords.alt.deg))
    header.insert("EACHSNAP", ("AIRMASS", airmass), after=True)

    # Compute Moon coords
    moon_eq = get_moon(time=datobs, location=site_location)
    moon_hour_angle = sidereal - moon_eq.ra
    if moon_hour_angle.deg < 0:
        moon_hour_angle += 360 * units.deg

    # Add Comment keywords
    header.insert("OBJECT", ("COMMENT", "Converted to normal format using python"), after=True)
    comment = f"{airmass} airmass of the source"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    moon = get_moon(datobs)
    moon_posang = equatorial_coords.position_angle(moon)
    comment = f"{moon_posang.deg} Pos.angle  of Moon, degrees"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    moon_dist = equatorial_coords.separation(moon)
    comment = f"{moon_dist.deg} distance to Moon, degrees"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    sun = get_sun(datobs)
    elongation = sun.separation(moon)
    phase = np.arctan2(sun.distance*np.sin(elongation), moon.distance - sun.distance*np.cos(elongation)).value
    phase = abs(np.pi-phase) / np.pi
    comment = f"{phase} Moon illuminated fraction"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    comment = f"{int(moon_hour_angle.hms[0])} {int(moon_hour_angle.hms[1])} {int(moon_hour_angle.hms[2])}  "
    comment += f"{int(moon_eq.dec.dms[0])} {int(moon_eq.dec.dms[1])} {int(moon_eq.dec.dms[2])}"
    comment += "   Hour Angle and Dec of MOON"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    comment = f"{int(moon_eq.ra.hms[0])} {int(moon_eq.ra.hms[1])} {int(moon_eq.ra.hms[2])}  "
    comment += f"{int(moon_eq.dec.dms[0])} {int(moon_eq.dec.dms[1])} {int(moon_eq.dec.dms[2])}"
    comment += "   RA and Dec of MOON"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    comment = f"{int(hour_angle.hms.h)} {int(hour_angle.hms.m)} {int(hour_angle.hms.s)}  "
    comment += f"{dec_dms[0]} {dec_dms[1]} {dec_dms[2]} Hour Angle and Dec of SOURCE"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    comment = f"{ra_hms[0]} {ra_hms[1]} {ra_hms[2]}  "
    comment += f"{dec_dms[0]} {dec_dms[1]} {dec_dms[2]} pointing coords."
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    comment = f"{int(sidereal.hms.h)}h{int(sidereal.hms.m)}m{int(sidereal.hms.s)}s  Sidereal time"
    header.insert("OBJECT", ("COMMENT", comment), after=True)
    header["DATE"] = (datetime.date.today().isoformat(), "Date header was created")

    # Reverse image
    hdu[0].data = np.fliplr(hdu[0].data)

    outname = path_to_data / f"{fname.name[:fname.name.index('S')]}.{index:03}"
    hdu.writeto(outname, overwrite=True)


# Let's iterate over all FIT files
all_files = sorted(path_to_data.glob("*.FIT"))
pool = multiprocessing.Pool(4)
pool.map(process, all_files)
